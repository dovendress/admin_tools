# README #

These are standalone executables, queries, routines, etc.

### What is this repository for? ###

Store useful sql, bash, perl, etc. related to database/system administration
v1.0

### How do I get set up? ###

Requires linux and mysql cli

### Contribution guidelines ###

Follow the same spacing, newlines, etc. as detailed in the Migration Template in confluence: 
https://appetize.atlassian.net/l/c/PqzKmWN1

### Who do I talk to? ###

* Dov Endress